import torch
import io
import json
from torchvision import transforms
from PIL import Image
from classes import classes
from flask import Flask, jsonify, request

app = Flask(__name__)

model = torch.jit.load("caltech256-script.zip")

model.eval()

def transform_image(image_bytes):
    my_transforms = transforms.Compose([transforms.Resize(255),
                                        transforms.CenterCrop(224),
                                        transforms.ToTensor(),
                                        transforms.Normalize(
                                            [0.485, 0.456, 0.406],
                                            [0.229, 0.224, 0.225])])
    image = Image.open(io.BytesIO(image_bytes))
    return my_transforms(image).unsqueeze(0)


def get_prediction(image_bytes):
    tensor = transform_image(image_bytes=image_bytes)
    outputs = model.forward(tensor)
    _, y_hat = torch.max(outputs, 1)
    return formatText(classes[y_hat])

def formatText(string):
    string = string[4:]
    string = string.replace("-", " ")
    return string


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        filee = request.files['file']
        img_bytes = filee.read()
        class_name = get_prediction(image_bytes=img_bytes)
        return jsonify({'class_name': class_name})


if __name__ == '__main__':
    app.run()
    # print("Started")
